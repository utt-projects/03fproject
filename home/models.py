from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.

class UserPrueba(models.Model):
    first_name = models.CharField(max_length=16, default="Gost User", blank=True, null=True)
    last_name = models.CharField(max_length=16, default="Phantom User")
    age = models.IntegerField(default=1)
    weight = models.FloatField(default=1.5)
    status = models.BooleanField(default=True)
    timestamp = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.first_name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=32, default="Generic User", blank=True, null=True)
    age = models.IntegerField(default=0, blank=True, null=True)
    email = models.EmailField(default="user@email.com", blank=True, null=True)
    avatar = models.ImageField(upload_to="users/profiles", default="avatar.png", blank=True, null=True)
    bio = models.CharField(max_length=256, default="I Love this APP", blank=True, null=True)
    status = models.BooleanField(default=True)
    
    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def update_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()