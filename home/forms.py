from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"type":"text", "class":"form-control","placeholder":"Escribe tu username"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control","placeholer":"Escribe tu Password"}))


class UserForm(UserCreationForm):
    username = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}))
    password1 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}))
    password2 = forms.CharField(max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Confirma tu Password"}))
    first_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu First Name"}))
    last_name = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Last Name"}))
    email = forms.CharField(max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Email"}))
    
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
            "first_name",
            "last_name",
            "email"
        ]

        # widgets = {
        #     "username": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Username"}),
        #     "password": forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escribe tu Password"}),
        #     "email": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe tu Email"}),
        # }



class UpdateProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = "__all__"