from django.shortcuts import render, HttpResponse, redirect


from rest_framework import generics
import requests
from django.views import generic
from django.core import serializers

from core.models import Category

from .forms import CreateCategoryClientForm
from .serializers import CategorySerializer
# Create your views here.


class WSCategory(generic.View):
    template_name = None
    context = None
    queryset = None

    def get(self,request):
        self.queryset = Category.objects.filter(status=True)
        data = serializers.serialize('json', self.queryset)
        return HttpResponse(data, content_type="application/json")


class WSClient(generic.View):
    template_name = "api/ws_client.html"
    context = {}
    url = "http://localhost:8000/api/v2/list/categories/"
    response = None

    def get(self, request):
        self.response = requests.get(url=self.url)
        self.context = {
            "categories": self.response.json()
        }
        return render(request, self.template_name, self.context)



class CategoryListAPIView(generics.ListAPIView):
    queryset = Category.objects.filter(status=True)
    serializer_class = CategorySerializer



class CreateCategoryAPIView(generics.CreateAPIView):
    serializer_class = CategorySerializer
    


#### CLIENTS ####

class CreateCategoryClient(generic.View):
    template_name = "api/ccreate_category.html"
    context = {}
    url_api = "http://localhost:8000/api/v2/create/category/"
    form_class = CreateCategoryClientForm
    response = None
    payload = None 

    def get(self, request):
        self.context = {
            "form": self.form_class
        }
        return render(request,self.template_name, self.context)
    
    def post(self, request, *args, **kwargs):
        self.payload = {
            "category_name": request.POST["category_name"],
            "description": request.POST["description"],
            "status": request.POST["status"],
            "slug": request.POST["slug"],
            "user": request.POST["user"]
        }
        self.response = requests.post(url=self.url_api, data=self.payload)
        return redirect("api:ws_client")