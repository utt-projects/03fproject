

from django.urls import path

from api import views

app_name = "api"


urlpatterns = [
    path('ws/categories/', views.WSCategory.as_view(), name="ws_categories"),
    ###### CLIENT'S URLS #####
    path('ws/categories/client/', views.WSClient.as_view(), name="ws_client"),
    path('v2/client/create/category/', views.CreateCategoryClient.as_view(), name="api_create_category"),
    ##### APIS URLS #####
    path('v2/list/categories/', views.CategoryListAPIView.as_view(), name="api_list_category"),
    path('v2/create/category/', views.CreateCategoryAPIView.as_view(), name="api_create_category"),
]