



from django import forms

from core.models import Category


class CreateCategoryClientForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = "__all__"